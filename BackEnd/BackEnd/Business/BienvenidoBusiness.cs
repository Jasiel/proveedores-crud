﻿using BackEnd.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BackEnd.Business
{
    public class BienvenidoBusiness: BusinessBase
    {
        private BienvenidoRepository repository;

        public BienvenidoBusiness()
        {
            repository = new BienvenidoRepository(); ;
        }

        public void Get()
        {
            businessResult.Success = true;
            businessResult.Content = repository.Get();
            businessResult.Message = "";
        }
    }
}