﻿using BackEnd.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Xml;
using System.Xml.Serialization;

namespace BackEnd.Business
{
    public class BusinessBase
    {
        protected BusinessResult businessResult;
      
        public BusinessBase()
        {
            
            InitResult();
        }

        public BusinessResult GetResult()
        {
            return businessResult;
        }

        private void InitResult()
        {
            businessResult = new BusinessResult
            {
                Content = null,
                Message = "Error al procesar la solicitud",
                StatusCode = HttpStatusCode.InternalServerError,
                Success = false
            };
        }

    }
}