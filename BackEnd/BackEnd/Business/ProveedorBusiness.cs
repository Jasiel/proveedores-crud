﻿using BackEnd.Models;
using BackEnd.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BackEnd.Business
{
    public class ProveedorBusiness: BusinessBase
    {
        private ProveedorRepository repository;

        public ProveedorBusiness()
        {
            repository = new ProveedorRepository(); ;
        }

        public void Get(int indice, int cantidad)
        {
            businessResult.Success = true;
            businessResult.Content = repository.Get(indice, cantidad);
            businessResult.Message = "";
        }

        public void Insert(Proveedor data)
        {
            businessResult.Success = true;
            businessResult.Message = repository.Insert(data);
        }

        public void Update(Proveedor data)
        {
            businessResult.Success = true;
            businessResult.Message = repository.Update(data);
        }

        public void Delete(Proveedor data)
        {
            repository.Delete(data);
            businessResult.Success = true;
            businessResult.Message = "Se borro correctamente";
        }

        public void GetTotal()
        {
            businessResult.Success = true;
            businessResult.Content = repository.GetTotal();
            businessResult.Message = "";
        }
    }
}