﻿using BackEnd.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BackEnd.Repositories
{
    public class BienvenidoRepository
    {
        public Bienvenido Get()
        {
            return new Bienvenido() { Informacion = "Bienvenido Candidato 01", Version = "0.0.1"};
        }
    }
}