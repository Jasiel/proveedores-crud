﻿using BackEnd.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;

namespace BackEnd.Repositories
{
    public class ProveedorRepository
    {
        private List<Proveedor> proveedores;
        private string mensaje = "Informacion guardada correctamente.";
        public List<Proveedor> Get(int indice, int cantidad)
        {
            using (StreamReader r = new StreamReader(GetPath()))
            {
                string json = r.ReadToEnd();
                {
                    var total = JsonConvert.DeserializeObject<List<Proveedor>>(json).Count;
                    if (cantidad > (total - 1) - indice) 
                    {
                        cantidad = total - indice;
                    }
                    proveedores = JsonConvert.DeserializeObject<List<Proveedor>>(json).GetRange(indice, cantidad);
                }
            }

            return proveedores;
        }

        public string Insert(Proveedor data)
        {
            using (StreamReader r = new StreamReader(GetPath()))
            {
                string json = r.ReadToEnd();
                {
                    proveedores = JsonConvert.DeserializeObject<List<Proveedor>>(json);
                    if(!proveedores.Exists(x => x.Nombre == data.Nombre))
                    {
                        proveedores.Add(data);
                    } else
                    {
                        mensaje = "El proveedor ya existe, ingrese otro nombre";
                    }
                }
            }
            var jsonString = JsonConvert.SerializeObject(proveedores);
            File.WriteAllText(GetPath(), jsonString);
            return mensaje;
        }

        public string Update(Proveedor data)
        {

            using (StreamReader r = new StreamReader(GetPath()))
            {
                string json = r.ReadToEnd();
                {
                    proveedores = JsonConvert.DeserializeObject<List<Proveedor>>(json);
                    if (!proveedores.Exists(x => x.Nombre == data.Nombre && x.Id != data.Id))
                    {
                        foreach (var item in proveedores)
                        {

                            if (item.Id == data.Id)
                            {
                                item.Nombre = data.Nombre;
                                item.RazonSocial = data.RazonSocial;
                                item.Direccion = data.Direccion;
                            }
                        }
                    } else
                    {
                        mensaje = "El proveedor ya existe, ingrese otro nombre";
                    }
                }
            }
            var jsonString = JsonConvert.SerializeObject(proveedores);
            File.WriteAllText(GetPath(), jsonString);
            return mensaje;
        }

        public void Delete(Proveedor data)
        {
            using (StreamReader r = new StreamReader(GetPath()))
            {
                string json = r.ReadToEnd();
                {
                    proveedores = JsonConvert.DeserializeObject<List<Proveedor>>(json);
                    proveedores.RemoveAll(x => x.Id == data.Id);
                }
            }
            var jsonString = JsonConvert.SerializeObject(proveedores);
            File.WriteAllText(GetPath(), jsonString);
        }

        public Informacion GetTotal()
        {
            int total;
            using (StreamReader r = new StreamReader(GetPath()))
            {
                string json = r.ReadToEnd();
                {
                    total = JsonConvert.DeserializeObject<List<Proveedor>>(json).Count;
                }
            }

            return new Informacion() { Total = total };
        }

        public string GetPath()
        {
            return HostingEnvironment.MapPath(ConfigurationManager.AppSettings["DB"]);
        }
    }
}