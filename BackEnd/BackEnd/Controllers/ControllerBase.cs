﻿using BackEnd.Models;
using libx.log;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace BackEnd.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ControllerBase: ApiController
    {
        protected HttpResponseMessage controllerResponse;

        public ControllerBase()
        {
            controllerResponse = new HttpResponseMessage();
        }

        protected void PrintException(Exception e)
        {
            WebLog.Write(EventLogType.Error, string.Format("Excepcion [ {0} ]", e.Message));
            WebLog.Write(EventLogType.Error, string.Format("Traza [ {0} ]", e.StackTrace));
            SetStringContent(HttpStatusCode.InternalServerError, "Error al procesar la solicitud");
        }

        protected void PrintParseError()
        {
            using (var stream = new StreamReader(Request.Content.ReadAsStreamAsync().Result))
            {
                stream.BaseStream.Position = 0;
                /*WebLog.Write(EventLogType.Debug, string.Format("REQUEST BODY {0}", stream.ReadToEnd()));*/
            }
            /*WebLog.Write(EventLogType.Error, "Error al procesar la solicitud, ocurrió un error al analizar json");*/
            SetStringContent(HttpStatusCode.BadRequest, "Error al procesar la solicitud");
        }

        protected void PrintRequest()
        {
            using (var stream = new StreamReader(Request.Content.ReadAsStreamAsync().Result))
            {
                stream.BaseStream.Position = 0;
                /*WebLog.Write(EventLogType.Error, string.Format("REQUEST BODY {0}", stream.ReadToEnd()));*/
            }
        }

        protected void SetResultData(BusinessResult result)
        {
            if (result.Success)
            {
                if (result.Content == null)
                {
                    SetStringContent(result.Message);
                }
                else
                {
                    SetJsonContent(result.Content);
                }
            }
            else
            {
                SetStringContent(result.StatusCode, result.Message);
            }
        }

        protected void SetJsonContent(object value)
        {
            SetJsonContent(HttpStatusCode.OK, value);
        }

        protected void SetJsonContent(HttpStatusCode code, object value)
        {
            SetContent(code, new StringContent(
                JsonConvert.SerializeObject(value, new JsonSerializerSettings
                {
                    //ContractResolver = new CamelCasePropertyNamesContractResolver(),
                    Formatting = Formatting.None
                }), Encoding.UTF8, "application/json"));
        }

        protected void SetStringContent(string value)
        {
            SetStringContent(HttpStatusCode.OK, value);
        }

        protected void SetStringContent(HttpStatusCode code, string value)
        {
            SetContent(code, new StringContent(value, Encoding.UTF8, "text/plain"));
        }

        private void SetContent(HttpStatusCode code, HttpContent content)
        {
            controllerResponse.Content = content;
            controllerResponse.StatusCode = code;
        }
    }
}