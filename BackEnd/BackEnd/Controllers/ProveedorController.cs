﻿using BackEnd.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using BackEnd.Models;

namespace BackEnd.Controllers
{
    public class ProveedorController : ControllerBase
    {
        public ProveedorBusiness business;
        public ProveedorController()
        {
            business = new ProveedorBusiness();
        }

        public HttpResponseMessage Get(int indice, int cantidad)
        {
            try
            {
                business.Get(indice, cantidad);
                SetResultData(business.GetResult());
            }
            catch (Exception ex)
            {
                PrintException(ex);
            }
            finally
            {
                Dispose();
            }
            return controllerResponse;
        }

        [HttpGet]
        [Route("api/Proveedor/ObtenerInformacion")]
        public HttpResponseMessage ObtenerInformacion()
        {
            try
            {
                business.GetTotal();
                SetResultData(business.GetResult());
            }
            catch (Exception ex)
            {
                PrintException(ex);
            }
            finally
            {
                Dispose();
            }
            return controllerResponse;
        }

        [HttpPost]
        [Route("api/Proveedor/Guardar")]
        public HttpResponseMessage Guardar(Proveedor data)
        {
            try
            {
                business.Insert(data);
                SetResultData(business.GetResult());
            }
            catch (Exception ex)
            {
                PrintException(ex);
            }
            finally
            {
                Dispose();
            }
            return controllerResponse;
        }

        [HttpPost]
        [Route("api/Proveedor/Actualizar")]
        public HttpResponseMessage Actualizar(Proveedor data)
        {
            try
            {
                business.Update(data);
                SetResultData(business.GetResult());
            }
            catch (Exception ex)
            {
                PrintException(ex);
            }
            finally
            {
                Dispose();
            }
            return controllerResponse;
        }

        [HttpPost]
        [Route("api/Proveedor/Borrar")]
        public HttpResponseMessage Borrar(Proveedor data)
        {
            try
            {
                business.Delete(data);
                SetResultData(business.GetResult());
            }
            catch (Exception ex)
            {
                PrintException(ex);
            }
            finally
            {
                Dispose();
            }
            return controllerResponse;
        }
    }
}