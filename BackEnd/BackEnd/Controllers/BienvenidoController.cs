﻿using BackEnd.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace BackEnd.Controllers
{
    public class BienvenidoController: ControllerBase
    {
        public BienvenidoBusiness business;
        public BienvenidoController()
        {
            business = new BienvenidoBusiness();
        }

        public HttpResponseMessage Get()
        {
            try
            {
                business.Get();
                SetResultData(business.GetResult());
            }
            catch (Exception ex)
            {
                PrintException(ex);
            }
            finally
            {
                Dispose();
            }
            return controllerResponse;
        }
    }
}