﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BackEnd.Models
{
    public class Bienvenido
    {
        public string Informacion { get; set; }
        public string Version { get; set; }
    }
}