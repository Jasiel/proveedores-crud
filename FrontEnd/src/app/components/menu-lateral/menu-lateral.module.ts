import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MenuLateralComponent } from './menu-lateral.component';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { PiePaginaModule } from '../pie-pagina/pie-pagina.module';



@NgModule({
  declarations: [MenuLateralComponent],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PiePaginaModule
  ],
  exports: [MenuLateralComponent],
})
export class MenuLateralModule { }
