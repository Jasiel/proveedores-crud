import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { Bienvenido } from 'src/app/models/bienvenido';
import { BienvenidoService } from 'src/app/services/bienvenido.service';
import { CargandoService } from 'src/app/services/loading.service';
import { MensajeService } from 'src/app/services/messaging.service';

@Component({
  selector: 'app-menu-lateral',
  templateUrl: './menu-lateral.component.html',
  styleUrls: ['./menu-lateral.component.scss'],
})
export class MenuLateralComponent implements OnInit {

  bienvenido = new Bienvenido();

  constructor(
    private router: Router,
    private navCtrl: NavController,
    private bienvenidoService: BienvenidoService,
    private cargandoService: CargandoService,
    private mensajeService: MensajeService
    ) { }

  ngOnInit() {
    this.cargarBienvenida();
  }

  navigateTo(url: string) {
    this.navCtrl.navigateRoot(url);
  }


  cargarBienvenida() {
    this.cargandoService.present();
    this.bienvenidoService.obtener().then((data: Bienvenido) => {
      this.bienvenido = data;
      localStorage.setItem('bienvenido', JSON.stringify(data));
      this.cargandoService.dismiss();
    }).catch(e => {
      console.error(e);
      this.mensajeService.error('Hubo un error en obtener la informacion de la bienvenida');
      this.cargandoService.dismiss();
    });
  }

}
