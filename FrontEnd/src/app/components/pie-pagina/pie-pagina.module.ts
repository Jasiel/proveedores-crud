import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PiePaginaComponent } from './pie-pagina.component';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';



@NgModule({
  declarations: [PiePaginaComponent],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule
  ],
  exports: [PiePaginaComponent]
})
export class PiePaginaModule { }
