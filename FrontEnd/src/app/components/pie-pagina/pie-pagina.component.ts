import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-pie-pagina',
  templateUrl: './pie-pagina.component.html',
  styleUrls: ['./pie-pagina.component.scss'],
})
export class PiePaginaComponent implements OnInit {

  @Input() version: string;
  constructor() { }

  ngOnInit() {}

}
