import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { Bienvenido } from 'src/app/models/bienvenido';
import { BienvenidoService } from 'src/app/services/bienvenido.service';
import { CargandoService } from 'src/app/services/loading.service';
import { MensajeService } from 'src/app/services/messaging.service';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.page.html',
  styleUrls: ['./inicio.page.scss'],
})
export class InicioPage implements OnInit {

  bienvenido = new Bienvenido();
  constructor(
    private navCtrl: NavController,
    ) { }

  ngOnInit() {
    this.bienvenido = JSON.parse(localStorage.getItem('bienvenido'));
  }

  continuar() {
    this.navCtrl.navigateRoot("proveedor");
  }

}
