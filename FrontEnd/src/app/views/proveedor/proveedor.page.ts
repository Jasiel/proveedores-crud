import { Component, OnInit, ViewChild } from '@angular/core';
import { AlertController, IonInfiniteScroll, IonModal } from '@ionic/angular';
import { Informacion } from 'src/app/models/informacion';
import { Proveedor } from 'src/app/models/proveedor';
import { CargandoService } from 'src/app/services/loading.service';
import { MensajeService } from 'src/app/services/messaging.service';
import { ProveedorService } from 'src/app/services/proveedor.service';

@Component({
  selector: 'app-proveedor',
  templateUrl: './proveedor.page.html',
  styleUrls: ['./proveedor.page.scss'],
})
export class ProveedorPage implements OnInit {
  @ViewChild(IonModal) modal: IonModal;
  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;
  proveedores = Array<Proveedor>();
  proveedor = new Proveedor();
  esEditar = false;
  indiceActual = 0;
  indice = 0;
  cantidad = 5;
  total = 0;
  constructor(
    private proveedorService: ProveedorService,
    private cargandoService: CargandoService,
    private mensajeService: MensajeService,
    private alertController: AlertController
  ) { }

  ngOnInit() {
  }

  ionViewDidEnter() {
    this.obtener();
  }

  cancelar() {
    this.esEditar = false;
    this.limpiar();
    this.modal.dismiss(null, 'cancel');
  }

  confirmar() {
    if (this.esProveedorValido()) {
      if (this.esEditar) {
        this.actualizar();
      } else {
        this.proveedor.Id = Date.now().toString();
        this.guardar();
      }
    }
  }

  eliminar(indice: number) {
    this.proveedor = this.proveedores[indice];
    this.proveedores.splice(indice, 1);
    this.borrar();
  }

  editar(indice: number) {
    this.esEditar = true;
    this.indiceActual = indice;
    Object.assign(this.proveedor, this.proveedores[indice]);
    this.abrir()
  }

  abrir() {
    this.modal.present();
  }

  obtener() {
    this.cargandoService.present();
    this.proveedorService.obtener(this.indice, this.cantidad).then((data: Proveedor[]) => {
      this.proveedores = data;
      this.cargandoService.dismiss();
      this.obtenerInformacion();
    }).catch(e => {
      console.error(e);
      this.mensajeService.error('Hubo un error en obtener los proveedores.');
      this.cargandoService.dismiss();
    });
  }

  obtenerInformacion() {
    this.cargandoService.present();
    this.proveedorService.obtenerInformacion().then((data: Informacion) => {
      this.total = data.Total
      this.cargandoService.dismiss();
    }).catch(e => {
      console.error(e);
      this.mensajeService.error('Hubo un error en obtener los proveedores.');
      this.cargandoService.dismiss();
    });
  }


  guardar() {
    this.cargandoService.present();
    this.proveedorService.guardar(this.proveedor).then((data: string) => {
      this.mensajeService.success(data);
      this.proveedores.push(this.proveedor);
      this.cargandoService.dismiss();
      this.cancelar();
    }).catch(e => {
      console.error(e);
      this.mensajeService.error('Hubo un error al guardar.');
      this.cargandoService.dismiss();
    });
  }

  actualizar() {
    this.cargandoService.present();
    this.proveedorService.actualizar(this.proveedor).then((data: string) => {
      this.mensajeService.success(data)
      Object.assign(this.proveedores[this.indiceActual], this.proveedor);
      this.cargandoService.dismiss();
      this.cancelar();
    }).catch(e => {
      console.error(e);
      this.mensajeService.error('Hubo un error al actualizar.');
      this.cargandoService.dismiss();
    });
  }

  borrar() {
    this.cargandoService.present();
    this.proveedorService.borrar(this.proveedor).then(() => {
      this.mensajeService.success('Se borro correctamente el proveedor.')
      this.cargandoService.dismiss();
      this.limpiar();
    }).catch(e => {
      console.error(e);
      this.mensajeService.error('Hubo un error al borrar.');
      this.cargandoService.dismiss();
    });
  }

  esProveedorValido() {
    let exito = false;
    if (!this.proveedor.Nombre) {
      this.mensajeService.warning('Por favor ingrese un nombre.');
    } else if (!this.proveedor.RazonSocial) {
      this.mensajeService.warning('Por favor ingrese una razon social.');
    } else if (!this.proveedor.Direccion) {
      this.mensajeService.warning('Por favor ingrese una direccion.');
    } else if (this.proveedores.find(p => p.Nombre.toLowerCase() == this.proveedor.Nombre.toLowerCase() && p.Id != this.proveedor.Id)) {
      this.mensajeService.warning('El nombre del proveedor ya existe, ingrese otro nombre.');
    } else {
      exito = true;
    }
    return exito;
  }

  limpiar() {
    this.proveedor = new Proveedor();
  }

  async mostrarConfirmacion(indice: number) {
    const alert = await this.alertController.create({
      header: '¿Seguro que desea borrar el proveedor ' + this.proveedores[indice].Nombre + '?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
          },
        },
        {
          text: 'Aceptar',
          role: 'confirm',
          handler: () => {
            this.eliminar(indice);
          },
        },
      ],
    });

    await alert.present();
  }

  cargarMasProveedores(event: any) {
    setTimeout(() => {
      
      event.target.complete();
      this.actualizarPaginado();
      console.log('total '+this.total);
      console.log('indice '+this.indice);
      this.obtener();
      // App logic to determine if all data is loaded
      // and disable the infinite scroll
      if (this.cantidad >= this.total) {
        event.target.disabled = true;
      }
    }, 1000);
  }

  actualizarPaginado() {
    this.cantidad += 5;
  }
}
