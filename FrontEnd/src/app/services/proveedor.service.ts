import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Controllers } from '../enums/controllers';
import { Proveedor } from '../models/proveedor';

@Injectable({
  providedIn: 'root'
})
export class ProveedorService {
  private server = environment.server;
  constructor() { }

  public obtener(indice: number, cantidad: number) {
    return new Promise((resolve, reject) => {
      fetch(this.server + Controllers.Proveedor + '?indice=' + indice + '&cantidad='+cantidad, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
        }
      }).then(response => response.json()).then(data => {
        console.log(data)
        resolve(data);
      }).catch(e => reject(e));
    });
  }

  public obtenerInformacion() {
    return new Promise((resolve, reject) => {
      fetch(this.server + Controllers.Proveedor + '/ObtenerInformacion', {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
        }
      }).then(response => response.json()).then(data => {
        console.log(data)
        resolve(data);
      }).catch(e => reject(e));
    });
  }

  sincronizar(data: Proveedor[]) {
    return new Promise((resolve, reject) => {
      fetch(this.server + 'Proveedor/Sincronizar', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(data),
      }).then((response) => response.text()).then((data) => {
        resolve(true)
        console.log(data);
      }).catch(e => reject(e));
    });
  }

  guardar(data: Proveedor) {
    return new Promise((resolve, reject) => {
      fetch(this.server + 'Proveedor/Guardar', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(data),
      }).then((response) => response.text()).then((data) => {
        resolve(data)
        console.log(data);
      }).catch(e => reject(e));
    });
  }

  actualizar(data: Proveedor) {
    return new Promise((resolve, reject) => {
      fetch(this.server + 'Proveedor/Actualizar', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(data),
      }).then((response) => response.text()).then((data) => {
        resolve(data)
        console.log(data);
      }).catch(e => reject(e));
    });
  }

  borrar(data: Proveedor) {
    return new Promise((resolve, reject) => {
      fetch(this.server + 'Proveedor/Borrar', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(data),
      }).then((response) => response.text()).then((data) => {
        resolve(true)
        console.log(data);
      }).catch(e => reject(e));
    });
  }
}
