import { Injectable } from '@angular/core';
import { LoadingController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class CargandoService {

  isLoading = false;

  constructor(public loadingController: LoadingController) { }

  async present() {
    if(!this.isLoading)
    {
      this.isLoading = true;
      return await this.loadingController.create({
        duration: 120000,
        message:"Espere un momento..."
      }).then(a => {
        a.present().then(() => {
          if (!this.isLoading) {
            a.dismiss();
          }
        });
      });
    }
  }

  async dismiss() {
    if(this.isLoading)
    {
      this.isLoading = false;
      return await this.loadingController.dismiss();
    }
  }
}