import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Controllers } from '../enums/controllers';

@Injectable({
  providedIn: 'root'
})
export class BienvenidoService {

  private server = environment.server;
  constructor() { }

  public obtener() {
    return new Promise((resolve, reject) => {
      fetch(this.server + Controllers.Bienvenido, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
        }
      }).then(response => response.json()).then(data => {
        console.log(data)
        resolve(data);
      }).catch(e => reject(e));
    });
  }

  enviar() {
    return new Promise((resolve, reject) => {
      fetch(this.server + 'Materia', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ nombre: "test jasiel", cupo: 10 }),
      }).then((response) => response.text()).then((data) => {
        resolve(true)
        console.log(data);
      }).catch(e => reject(e));
    });
  }
}
