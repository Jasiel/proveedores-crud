# proveedores-crud

## Name

Proyecto CRUD diagnóstico de proveedores

## Description

Este proyecto diagnostico permite listar (usando infinitive scroll), guardar, editar y eliminar proveedores.

## Installation

1.- Instalar Visual Studio 2017(Community edition de preferencia ya que es gratis) o una versión mayor: https://visualstudio.microsoft.com/es/.

2.- Habilitar IIS (Internet Information Services) en el sistema operativo windows.

    2.1.- Para habilitar IIS, hay que ir a panel de control -> entrar en la opcion "desinstalar un programa" -> entrar en la opción "activar o           desactivar las características de Windows(se encuentra en la parte superior izquierda)" -> se abrirá una pequeña ventana donde tenemos que seleccionar todas la opciones de la carpeta y subcarpetas de "internet information services" -> una vez seleccionada las carpetas, hay que dar clic en aceptar y esperar a que Windows instale IIS -> una vez terminado el proceso de instalación se ocupara reiniciar Windows.

3.- Instalar git: https://git-scm.com/

4.- Instalar Nodejs: https://nodejs.org/es/

5.- Instalar Visual Studio Code: https://code.visualstudio.com/

## Usage

BackEnd

1.- Abrir consola de comandos de Windows cmd y descargar repositorio de git usando el comando: git clone https://gitlab.com/Jasiel/proveedores-crud.git

2.- Abrir Visual Studio 2017 o una versión mayor y abrir el proyecto llamado BackEnd.sln.

3.- Compilar proyecto BackEnd (dar clic derecho en la solución Backend y seleccionar la opción recompilar) en el VS.

4.- Una vez compilado dar clic derecho en el proyecto BackEnd y seleccionar la opción propiedades.

5.- Una vez abierta la pantalla de propiedades dirigirse a la opción de "Web" -> en el apartado de "servidores" seleccionar la opción del combo llamada "IIS local" -> después hay que dar clic en el botón "crear directorio virtual" -> aparecerá una ventana diciendo que se creó el directorio virtual correctamente.

FrontEnd

1.- Abrir consola de comandos de Windows cmd y descargar el repositorio de git usando el comando: git clone https://gitlab.com/Jasiel/proveedores-crud.git

2.- Abrir VS Code -> abrir carpeta FrontEnd (se encuentra dentro del repositorio de git https://gitlab.com/Jasiel/proveedores-crud.git).

3.- Abrir consola de comandos en el VS code -> escribir el comando npm install -> dar clic en enter -> esperar a que instale dependencias.

4.- Después ahi mismo en la consola de comandos del VS code, escribir el comando: npm install -g @ionic/cli -> dar clic en enter -> esperar a que instale dependencias.

5.- Después ahí mismo en la consola de comandos escribir el comando: ionic serve -> dar clic en enter -> esperar a que se compile y se abra el navegador.

## Support
Cualquier inconveniente en la ambientación de este proyecto, favor de contactarme al siguiente correo: jasieldavid@hotmail.com

